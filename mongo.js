/*

db.products.insertMany([
    {
        "name": "iPhone X",
        "price": 30000,
        "isActive": true,
    },

    {
        "name": "Samsung Galaxy S21",
        "price": 51000,
        "isActive": true,
    },
    {
        "name": "Razer Blackshark V2X",
        "price": 2800,
        "isActive": false,
    },
    {
        "name": "RAKK Gaming Mouse",
        "price": 1800,
        "isActive": true,
    },
    {
        "name": "Razer Mechanical Keyboard",
        "price": 4000,
        "isActive": true
    }
])

*/

/*

Query Operators allow us to expand our queries and define conditions
    $gt, $lt, $gte, $lte
    FYI: highlight line of code to run only that query
*/
// $gt (Greater than : >)

db.products.find({price: {$gt: 3000}})
// $lt (Less than : <)

db.products.find({price: {$lt: 3000}})
// $gte (greater than or equal to)

db.products.find({price: {$gte: 30000}})
// $lte (less than or equal to)

db.products.find({price: {$lte: 2800}})


// $regex is a query operator allow us to find documents which match characters/patten of the characters


db.users.find({firstName:{$regex: 'O'}})

// $options case insensitive
db.users.find({firstName:{$regex:'o', $options: '$i'}})

// partial matches
db.products.find({name:{$regex: 'phone', $options: '$i'}})

// find users where email have the word "web"
db.users.find({email:{$regex: 'web', $options: '$i'}})

// find product name that has name razer and rakk
db.products.find({name:{$regex: 'razer', $options: '$i'}})
db.products.find({name:{$regex: 'rakk', $options: '$i'}})

// $or &and 

db.products.find({$or:[{name:{$regex:'x',$options:'$i'}},{price:{$lte:10000}}]})
db.products.find({$or:[{name:{$regex:'x',$options:'$si'}},{price:{$gte:30000}}]})

// and

db.products.find({$and:[{name:{$regex:'razer',$options:'$i'}},{price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex:'x',$options:'$si'}},{price:{$gte:30000}}]})
db.users.find({$and:[{lastName:{$regex:'w', $options:'$i'}}, {isAdmin:false}]})
db.users.find({$and:[{firstName:{$regex:'a', $options:'$i'}}, {isAdmin:true}]})




// Field Projection - show/hide certain properties/fields
// db.collection.find({query}, {projection})
// 0 means hide
// 1 means show

db.users.find({}, {_id:0, password:0})
// _id needs to be zero 0
db.users.find({isAdmin:true}, {_id:0, email:1})
db.users.find({isAdmin:false}, {firstName:1, lastName:1})
db.products.find({price:{$gte:10000}}, {_id:0, name:1, price:1})












